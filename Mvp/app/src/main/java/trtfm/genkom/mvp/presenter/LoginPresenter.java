package trtfm.genkom.mvp.presenter;

/**
 * Created by osmanasar on 23.05.2018.
 */

public interface LoginPresenter {
    void perform(String name,String pass);
}
