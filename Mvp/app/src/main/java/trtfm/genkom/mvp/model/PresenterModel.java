package trtfm.genkom.mvp.model;

import android.text.TextUtils;

import trtfm.genkom.mvp.presenter.LoginPresenter;
import trtfm.genkom.mvp.views.LoginView;

/**
 * Created by osmanasar on 23.05.2018.
 */

public class PresenterModel implements LoginPresenter {

    LoginView view;

    public PresenterModel(LoginView view2){
        this.view = view2;
    }

    @Override
    public void perform(String name, String pass) {
        if(TextUtils.isEmpty(name) || TextUtils.isEmpty(pass)){
            view.validation();
        }else{
            if(name.equals("osman") && pass.equals("123456")){
                view.success();
            }
            else{
                view.error();
            }
        }
    }
}
