package trtfm.genkom.mvp.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import trtfm.genkom.mvp.R;
import trtfm.genkom.mvp.model.PresenterModel;
import trtfm.genkom.mvp.presenter.LoginPresenter;
import trtfm.genkom.mvp.views.LoginView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,LoginView{

    EditText et1 , et2 ;
    Button btn;
    LoginPresenter view2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et1 = (EditText)findViewById(R.id.editText);
        et2 = (EditText)findViewById(R.id.editText2);
        btn = (Button)findViewById(R.id.button);
        btn.setOnClickListener(this);
        view2 = new PresenterModel(MainActivity.this);
    }

    @Override
    public void onClick(View view) {
        String name = et1.getText().toString();
        String pass = et2.getText().toString();
        view2.perform(name,pass);
    }

    @Override
    public void validation() {
        System.out.println("Boş bırakılamaz");

    }

    @Override
    public void success() {
        System.out.println("Başarılı");
    }

    @Override
    public void error() {
        System.out.println("Hatalı");
    }
}
