package trtfm.genkom.mvp.views;

/**
 * Created by osmanasar on 23.05.2018.
 */

public interface LoginView {
     void validation();
     void success();
     void error();
}
